package kode.boot.logging.autoconfigure;

import kode.boot.logging.Logging;
import kode.boot.logging.LoggingInfo;
import kode.boot.logging.LoggingListener;
import kode.boot.logging.aop.LoggingAdvisor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LoggingAutoConfigurationTest {

	private final ApplicationContextRunner contextRunner = new ApplicationContextRunner()
			.withConfiguration(AutoConfigurations.of(AopAutoConfiguration.class, LoggingAutoConfiguration.class));

	private static final List<String> logs = new ArrayList<>();

	@Test
	void testLoggingEnable() {
		this.contextRunner
				.withUserConfiguration(TestConfiguration.class)
				.run((context) -> {
					LoggingAdvisor advisor = context.getBean(LoggingAdvisor.class);
					var matches = advisor.matches(TestClass.class.getMethod("testEnable"), TestClass.class);
					assertTrue(matches);
				});
	}

	@Test
	void testLoggingDisable() {
		this.contextRunner
				.withUserConfiguration(TestConfiguration.class)
				.run((context) -> {
					LoggingAdvisor advisor = context.getBean(LoggingAdvisor.class);
					var matches = advisor.matches(TestClass.class.getMethod("testDisable"), TestClass.class);
					assertFalse(matches);
				});
	}

	@Test
	void testLog() {
		this.contextRunner
				.withUserConfiguration(TestConfiguration.class)
				.run((context) -> {
					TestClass testClass = context.getBean(TestClass.class);

					testClass.testEnable();
					testClass.testDisable();

					assertTrue(logs.stream().anyMatch(c -> c.contains("logBeforeInvoked")));
					assertTrue(logs.stream().anyMatch(c -> c.contains("logAfterInvoked")));
					assertTrue(logs.stream().allMatch(c -> c.contains("testEnable")));
					assertFalse(logs.stream().anyMatch(c -> c.contains("testDisable")));
				});
	}

	@Configuration
	protected static class TestConfiguration {

		@Bean
		public LoggingListener listener() {
			return new TestLoggingListener();
		}

		@Bean
		public TestClass testClass() {
			return new TestClass();
		}
	}

	static class TestLoggingListener implements LoggingListener {
		@Override
		public void onLoggingBefore(LoggingInfo info) {
			logs.add(info.getMethod().getName() + " - " + info.getLogBefore());
		}

		@Override
		public void onLoggingAfter(LoggingInfo info) {
			logs.add(info.getMethod().getName() + " - " + info.getLogAfter());
		}
	}

	static class TestClass {

		@Logging(before = "logBeforeInvoked", after = "logAfterInvoked")
		public void testEnable() {
		}

		@Logging(enabled = false)
		public void testDisable() {
		}
	}
}