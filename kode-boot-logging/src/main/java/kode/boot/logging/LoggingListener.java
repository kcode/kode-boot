package kode.boot.logging;

/**
 * LoggingListener
 *
 * @author Stark
 * @date 2018/12/20
 */
public interface LoggingListener {

	default void onLoggingBefore(final LoggingInfo info) {
	}

	default void onLoggingAfter(final LoggingInfo info) {
	}
}
