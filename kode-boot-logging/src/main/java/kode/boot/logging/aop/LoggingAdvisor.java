package kode.boot.logging.aop;

import kode.boot.logging.LoggingResolver;
import lombok.Getter;
import lombok.Setter;
import org.springframework.aop.support.StaticMethodMatcherPointcutAdvisor;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class LoggingAdvisor extends StaticMethodMatcherPointcutAdvisor {

	private static final long serialVersionUID = -5909341812667936469L;
	private List<LoggingResolver> resolvers = new ArrayList<>();

	@Override
	public boolean matches(Method method, Class<?> targetClass) {
		return resolvers.stream().anyMatch(r -> r.support(targetClass, method));
	}

}
