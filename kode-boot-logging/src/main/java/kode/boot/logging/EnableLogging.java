package kode.boot.logging;

import kode.boot.logging.autoconfigure.LoggingAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;

import java.lang.annotation.*;

/**
 * EnableLogging
 *
 * @author Stark
 * @date 2018/12/24
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@ImportAutoConfiguration(LoggingAutoConfiguration.class)
public @interface EnableLogging {
}
