package kode.boot.logging.autoconfigure;

import kode.boot.logging.LoggingInfo;
import kode.boot.logging.LoggingListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DefaultLoggingListener
 *
 * @author Stark
 * @date 2018/12/24
 */
public class DefaultLoggingListener implements LoggingListener {

	@Override
	public void onLoggingBefore(LoggingInfo info) {
		if (info.getLogBefore() != null && !"".equals(info.getLogBefore().trim())) {
			Logger logger = LoggerFactory.getLogger(info.getTarget());
			logger.debug(info.getLogBefore());
		}
	}

	@Override
	public void onLoggingAfter(LoggingInfo info) {
		if (info.getLogAfter() != null && !"".equals(info.getLogAfter().trim())) {
			Logger logger = LoggerFactory.getLogger(info.getTarget());
			logger.debug(info.getLogAfter());
		}
	}
}
