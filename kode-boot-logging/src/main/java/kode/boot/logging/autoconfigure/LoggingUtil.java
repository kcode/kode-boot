package kode.boot.logging.autoconfigure;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

public class LoggingUtil {

	public static String joinWithoutEmpty(String separator, String... array) {
		return Arrays.stream(array)
				.filter(c -> c != null && !"".equals(c.trim()))
				.reduce((a, b) -> a + separator + b)
				.orElse("");
	}

	// remove address
	public static final String X_FORWARDED_FOR = "x-forwarded-for";


	/** 获取当前处理的请求 */
	public static HttpServletRequest getHttpServletRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	}

	/**
	 * 获取请求头
	 *
	 * @param request 请求信息
	 * @return 请求头键值
	 */
	public static Map<String, String> getHeaders(HttpServletRequest request) {
		var result = new HashMap<String, String>();

		Enumeration<String> temp = request.getHeaderNames();
		while (temp != null && temp.hasMoreElements()) {
			String key = temp.nextElement();
			result.put(key, request.getHeader(key));
		}

		return result;
	}

	/**
	 * 获取用户真实 IP 地址
	 * <p>
	 * 不使用 <code>request.getRemoteAddr()</code> 的原因是有可能用户使用了代理软件方式避免真实IP地址,
	 * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？
	 * 答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。
	 * <p>
	 * 如：X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130,
	 * 192.168.1.100
	 * <p>
	 * 用户真实IP为： 192.168.1.110
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader(X_FORWARDED_FOR);
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
			if (ip.equals("127.0.0.1") || ip.equals("0:0:0:0:0:0:0:1")) {
				//根据网卡取本机配置的IP
				InetAddress inet = null;
				try {
					inet = InetAddress.getLocalHost();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				if (inet != null) {
					ip = inet.getHostAddress();
				}
			}
		}
		//对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
		if (ip != null && ip.length() > 15) {
			//"***.***.***.***".length() = 15
			if (ip.indexOf(",") > 0) {
				ip = ip.substring(0, ip.indexOf(","));
			}
		}
		return ip;
	}

}
