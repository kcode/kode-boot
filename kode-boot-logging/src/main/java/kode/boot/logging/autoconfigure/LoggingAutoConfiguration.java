package kode.boot.logging.autoconfigure;

import kode.boot.logging.*;
import kode.boot.logging.aop.LoggingAdvice;
import kode.boot.logging.aop.LoggingAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * WebLoggingAutoConfiguration
 *
 * @author Stark
 * @date 2018/12/24
 */
@Configuration
@ConditionalOnClass(Logging.class)
public class LoggingAutoConfiguration {

	@Autowired
	private ApplicationContext applicationContext;

	@Bean
	public LoggingResolver loggingResolver() {
		return new DefaultLoggingResolver();
	}

	@Bean
	@ConditionalOnMissingBean(LoggingListener.class)
	public LoggingListener loggingListener() {
		return new DefaultLoggingListener();
	}

	@Bean
	public LoggingAdvice loggingAdvice(ApplicationContext applicationContext,
	                                   List<LoggingListener> listeners,
	                                   List<LoggingResolver> resolvers) {
		var advice = new LoggingAdvice();
		advice.setEventPublisher(applicationContext);
		advice.setResolvers(resolvers);
		advice.setListeners(listeners);
		return advice;
	}

	@Bean
	public LoggingAdvisor loggingAdvisor(LoggingAdvice advice, List<LoggingResolver> resolvers) {
		var advisor = new LoggingAdvisor();
		advisor.setResolvers(resolvers);
		advisor.setAdvice(advice);
		return advisor;
	}

}
