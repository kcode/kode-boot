package kode.boot.logging;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * Logging，默认退出方法时输出当前日志
 *
 * @author Stark
 * @date 2018/12/15
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Logging {

	@AliasFor("after")
	String value() default "";

	String before() default "";

	@AliasFor("value")
	String after() default "";

	String[] details() default {};

	boolean enabled() default true;
}
