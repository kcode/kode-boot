package kode.boot.logging;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.core.Ordered;

import java.lang.reflect.Method;

/**
 * LoggingResolver
 *
 * @author Stark
 * @date 2018/12/20
 */
public interface LoggingResolver extends Ordered {

	/**
	 * 方法调用是否支持日志切面输出
	 *
	 * @param targetClass 检查类
	 * @param method      检查方法
	 * @return 是否支持
	 */
	boolean support(Class<?> targetClass, Method method);

	/**
	 * 处理主要信息，其他有默认处理完成
	 *
	 * @param invocation 切入点
	 * @return 日志信息
	 */
	LoggingInfo resolve(MethodInvocation invocation);

}
