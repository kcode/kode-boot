package kode.boot.file.api;

/**
 * FileValidateException
 *
 * @author Stark
 * @date 2019/1/3
 */
public class FileValidateException extends FileException {

	private static final long serialVersionUID = 4810229805976744752L;

	public FileValidateException(String message) {
		super(message);
	}

	public FileValidateException(Throwable throwable) {
		super(throwable);
	}

	public FileValidateException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
