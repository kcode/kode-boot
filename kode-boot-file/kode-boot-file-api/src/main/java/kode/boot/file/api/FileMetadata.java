package kode.boot.file.api;

import kode.base.core.entity.AuditableSupport;
import kode.base.core.entity.GenericEntity;
import kode.base.core.util.FileUtil;
import kode.base.core.util.Validate;
import org.springframework.web.multipart.MultipartFile;

public interface FileMetadata<ID> extends GenericEntity<ID>, AuditableSupport<ID> {

	/**
	 * display filename, original filename when uploaded, or renamed
	 */
	String getName();

	void setName(String name);

	/**
	 * File Type is basically synonymous with the term File Format.
	 * Both describe the way and manner in which different information is stored on your computer.
	 * The media information inside a file is encoded in a particular way,
	 * depending on the file type. This information is given to the device
	 * to tell how the file in question can be opened.
	 * <p>
	 * 文件的实际类型
	 */
	default String getType() {
		return null;
	}

	default void setType(String type) {
	}

	/**
	 * file extension, '.' included
	 */
	String getExtension();

	void setExtension(String extension);


	/**
	 * content type by Paths.probeContentType
	 */
	default String getContentType() {
		return null;
	}

	default void setContentType(String contentType) {
	}


	/**
	 * image width only for image
	 */
	default Integer getWidth() {
		return null;
	}

	default void setWidth(Integer width) {
	}

	default Integer getHeight() {
		return null;
	}

	default void setHeight(Integer height) {
	}

	/**
	 * web director path of file. for folder feature. default from root path('/')
	 */
	default String getFilePath() {
		return "/";
	}

	default void setFilePath() {
	}


	/**
	 * location is where the file is stored on disk from file base path, starts with {@link java.io.File#separator}.
	 */
	default String getLocation() {
		return null;
	}

	default void setLocation(String location) {
	}

	/**
	 * url for request
	 */
	default String getUrl() {
		return null;
	}

	default void setUrl() {
	}


	default String getFingerprint() {
		return null;
	}

	default void setFingerprint(String fingerprint) {
	}


	long getSize();

	void setSize(long size);

	default int getAccess() {
		return -1;
	}

	default void setAccess(int access) {
	}


	default Object getExtras() {
		return null;
	}

	default void setExtras(Object extras) {
	}

	default Object getExtra(String key) {
		return null;
	}

	/**
	 * set extension and name.
	 */
	default void setNameAndExtension(String name) {
		setName(name);
		setExtension(FileUtil.getFileExtension(name));
	}

	static <ID> void load(FileMetadata<ID> fileMetadata, MultipartFile multipartFile) {
		Validate.notNull(fileMetadata);
		Validate.notNull(multipartFile);

		fileMetadata.setContentType(multipartFile.getContentType());
		fileMetadata.setNameAndExtension(multipartFile.getOriginalFilename());
		fileMetadata.setSize(multipartFile.getSize());
	}
}