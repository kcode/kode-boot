package kode.boot.file.api;


import kode.base.core.service.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * FileService
 *
 * @author Stark
 * @date 2018/12/25
 */
public interface FileService<T extends FileMetadata<ID>, ID> extends Service {

	// read file
	//-----------------------

	long getFileSize(String location) throws IOException;

	/**
	 * read file by id
	 *
	 * @throws FileException when file not found
	 */
	InputStream readFile(ID id);

	/**
	 * read file by meta info
	 *
	 * @throws FileException when file not found
	 */
	InputStream readFile(T meta);

	/**
	 * read file by location
	 */
	InputStream readFile(String location);


	void writeToStream(ID id, OutputStream stream) throws IOException;

	/**
	 * read file by id, then write to the output stream
	 *
	 * @param id     meta info id
	 * @param stream output stream
	 * @param skip   skip
	 * @throws FileException in case of file not found
	 * @throws IOException   in case of I/O errors
	 */
	void writeToStream(ID id, OutputStream stream, long skip) throws IOException;

	void writeToStream(ID id, OutputStream stream, long skip, long end) throws IOException;


	void writeToStream(String location, OutputStream stream) throws IOException;

	/**
	 * read file by location  and transfer to output stream
	 */
	void writeToStream(String location, OutputStream stream, long skip) throws IOException;

	void writeToStream(String location, OutputStream stream, long skip, long end) throws IOException;

	// save file
	//-----------------------

	T saveFileAndMeta(String base64Str, T fileMeta, FileValidator validator) throws IOException;

	/**
	 * save file, save/update file meta. should update size/location/fingerprint/date created(if null)/content type(if null)
	 *
	 * @param inputStream file input stream
	 * @param fileMeta    meta info
	 * @param validator   file validator
	 * @return updated meta info with several info that will only be available at completion
	 * @throws IOException i/o errors
	 */
	T saveFileAndMeta(InputStream inputStream, T fileMeta, FileValidator validator) throws IOException;

	T saveFileAndMeta(InputStream inputStream, T fileMeta) throws IOException;

	void saveFile(String base64Str, T fileMeta, FileValidator validator) throws IOException;

	void saveFile(InputStream inputStream, T fileMeta, FileValidator validator) throws IOException;

	void saveFile(InputStream inputStream, T fileMeta) throws IOException;

	// delete file
	//-----------------------

	boolean deleteFile(ID key);

	boolean deleteFile(String location);

}