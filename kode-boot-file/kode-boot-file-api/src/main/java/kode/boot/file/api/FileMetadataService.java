package kode.boot.file.api;

import kode.base.core.service.CrudService;
import kode.base.core.service.Service;

import java.util.Optional;

/**
 * FileMetadataService
 *
 * @author Stark
 * @date 2018/12/25
 */
public interface FileMetadataService<T extends FileMetadata<ID>, ID> extends CrudService<T, ID>, Service {

	default Optional<T> getByFingerprint(String fingerprint) {
		return Optional.empty();
	}

	T newInstance();

}
