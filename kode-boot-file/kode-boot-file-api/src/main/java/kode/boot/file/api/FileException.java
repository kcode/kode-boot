package kode.boot.file.api;

/**
 * FileException
 *
 * @author Administrator
 * @date 2019/1/3
 */
public class FileException extends RuntimeException {

	private static final long serialVersionUID = -7690780762727705783L;

	public FileException(String message) {
		super(message);
	}

	public FileException(Throwable throwable) {
		super(throwable);
	}

	public FileException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
