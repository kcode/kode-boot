package kode.boot.file.simple;

import kode.base.jpa.common.JpaGenericRepo;

import java.util.Optional;

/**
 * SimpleFileMetadataRepo
 *
 * @author Stark
 * @date 2019/1/5
 */
public interface SimpleFileMetadataRepo extends JpaGenericRepo<SimpleFileMetadata, Long> {

	Optional<SimpleFileMetadata> findFirstByFingerprint(String fingerprint);

}
