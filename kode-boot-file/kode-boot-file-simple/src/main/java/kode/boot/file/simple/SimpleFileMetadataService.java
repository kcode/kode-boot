package kode.boot.file.simple;

import kode.boot.file.api.FileMetadataService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * SimpleFileMetadataService
 *
 * @author Stark
 * @date 2019/1/5
 */
@Getter
@Setter
public class SimpleFileMetadataService implements FileMetadataService<SimpleFileMetadata, Long> {

	@Autowired
	private SimpleFileMetadataRepo metadataRepo;

	@Override
	public SimpleFileMetadata newInstance() {
		return new SimpleFileMetadata();
	}

	@Override
	public Optional<SimpleFileMetadata> getByFingerprint(String fingerprint) {
		return metadataRepo.findFirstByFingerprint(fingerprint);
	}

	@Override
	public boolean existsById(Long aLong) {
		return false;
	}

	@Override
	public void deleteById(Long id) {
		metadataRepo.deleteById(id);
	}

	@Override
	public void delete(SimpleFileMetadata entity) {
		metadataRepo.delete(entity);
	}

	@Override
	public void deleteAll(Iterable<? extends SimpleFileMetadata> entities) {
		metadataRepo.deleteAll(entities);
	}

	@Override
	public void deleteAll() {
		metadataRepo.deleteAll();
	}

	@Override
	public <S extends SimpleFileMetadata> S insert(S entity) {
		return metadataRepo.save(entity);
	}

	@Override
	public <S extends SimpleFileMetadata> Iterable<S> insertAll(Iterable<S> entities) {
		return metadataRepo.saveAll(entities);
	}

	@Override
	public Optional<SimpleFileMetadata> getById(Long id) {
		return metadataRepo.findById(id);
	}

	@Override
	public Iterable<SimpleFileMetadata> getAll() {
		return metadataRepo.findAll();
	}

	@Override
	public Iterable<SimpleFileMetadata> getAllById(Iterable<Long> longs) {
		return metadataRepo.findAllById(longs);
	}

	@Override
	public long count() {
		return metadataRepo.count();
	}

	@Override
	public <S extends SimpleFileMetadata> S saveOrUpdate(S entity) {
		return metadataRepo.save(entity);
	}

	@Override
	public <S extends SimpleFileMetadata> S update(S entity) {
		return metadataRepo.save(entity);
	}

	@Override
	public <S extends SimpleFileMetadata> Iterable<S> updateAll(Iterable<S> entities) {
		return metadataRepo.saveAll(entities);
	}
}
