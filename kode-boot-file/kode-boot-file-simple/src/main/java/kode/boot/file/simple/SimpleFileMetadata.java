package kode.boot.file.simple;

import kode.base.jpa.common.JpaGenericEntity;
import kode.boot.file.api.FileMetadata;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.OffsetDateTime;

/**
 * SimpleFileMetadata
 *
 * @author Stark
 * @date 2019/1/5
 */
@Getter
@Setter
@Entity
@Table(name = "file_metadata")
public class SimpleFileMetadata extends JpaGenericEntity<Long> implements FileMetadata<Long>, Serializable {

	private static final long serialVersionUID = -6118793809375419542L;

	private String name;
	private String extension;
	private String type;
	private String contentType;
	private String fingerprint;
	private String location;
	private String url;
	private long size;

	private Long createdBy;
	private OffsetDateTime dateCreated;
	private Long updatedBy;
	private OffsetDateTime dateUpdated;

}
