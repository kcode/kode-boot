package kode.boot.file.autoconfigure;

import kode.boot.file.api.FileMetadataService;
import kode.boot.file.api.FileService;
import kode.boot.file.local.LocalFileService;
import kode.boot.file.simple.SimpleFileMetadataRepo;
import kode.boot.file.web.FileController;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

/**
 * FileServiceAutoConfigurationTest
 *
 * @author Stark
 * @date 2019/1/5
 */
class FileServiceAutoConfigurationTest {

	private final ApplicationContextRunner contextRunner = new ApplicationContextRunner()
			.withPropertyValues("file.web.enabled:false");

	@Test
	void configFileService_local() {
		this.contextRunner
				.withConfiguration(AutoConfigurations.of(FileServiceAutoConfiguration.class))
				.run((context) -> {
					FileService fileService = context.getBean(FileService.class);
					assertNotNull(fileService);
					assertTrue(fileService instanceof LocalFileService);
					assertEquals(fileService, context.getBean("localFileService"));
				});
	}

	@Test
	void configFileService_mock() {
		this.contextRunner
				.withUserConfiguration(MockFileServiceConfig.class, FileServiceAutoConfiguration.class)
				.run((context) -> {
					FileService fileService = context.getBean(FileService.class);
					assertNotNull(fileService);
					assertFalse(fileService instanceof LocalFileService);
					assertEquals(fileService, context.getBean("myFileService"));
				});
	}

	@Test
	void configMetadataService_withoutDataSource() {
		this.contextRunner
				.withConfiguration(AutoConfigurations.of(FileServiceAutoConfiguration.class))
				.run((context) -> assertThrows(NoSuchBeanDefinitionException.class,
						() -> context.getBean(FileMetadataService.class)));
	}

	@Test
	void configMetadataService_jpa() {
		this.contextRunner
				.withConfiguration(AutoConfigurations.of(DataSourceAutoConfiguration.class))
				.withUserConfiguration(FileServiceAutoConfiguration.class)
				.run((context) -> assertThrows(NoSuchBeanDefinitionException.class,
						() -> context.getBean(FileMetadataService.class)));
	}

	@Nested
	@DataJpaTest
	@ContextConfiguration(classes = {DataSourceAutoConfiguration.class, FileServiceAutoConfiguration.class})
	class JpaMetadataServiceTest {
		@Autowired
		ApplicationContext applicationContext;
		@Autowired
		SimpleFileMetadataRepo repo;
		@Autowired
		FileMetadataService service;

		@Test
		void simpleFileMetadataRepo() {
			assertNotNull(repo);
			SimpleFileMetadataRepo temp = applicationContext.getBean(SimpleFileMetadataRepo.class);
			assertNotNull(temp);
			assertEquals(repo, temp);
		}

		@Test
		void simpleFileMetadataService() {
			assertNotNull(service);

			Map<String, FileMetadataService> map = applicationContext.getBeansOfType(FileMetadataService.class);
			assertFalse(map.isEmpty());
			assertEquals(1, map.size());

			assertTrue(map.containsKey("simpleFileMetadataService"));
			assertTrue(map.containsValue(service));
		}
	}

	@Nested
	@DataJpaTest
	@AutoConfigurationPackage
	@ContextConfiguration(classes = {DataSourceAutoConfiguration.class, MockMetadataServiceConfig.class, FileServiceAutoConfiguration.class})
	class JpaMockMetadataServiceTest {
		@Autowired
		ApplicationContext applicationContext;

		@Test
		void noJpaBean_mockMetadataService() {
			assertThrows(NoSuchBeanDefinitionException.class, () -> applicationContext.getBean(SimpleFileMetadataRepo.class));
		}
	}

	@Test
	void configWeb_enabled() {
		this.contextRunner
				.withConfiguration(AutoConfigurations.of(FileServiceAutoConfiguration.class, FileWebAutoConfiguration.class))
				.withUserConfiguration(MockFileServiceConfig.class, MockMetadataServiceConfig.class)
				.withPropertyValues("file.web.enabled:true")
				.run((context) -> {
					assertNotNull(context.getBean(FileWebAutoConfiguration.WebConfiguration.class));
					assertNotNull(context.getBean(FileController.class));
				});
	}

	@Test
	void configWeb_disabled() {
		this.contextRunner
				.withConfiguration(AutoConfigurations.of(FileServiceAutoConfiguration.class))
				.withUserConfiguration(MockMetadataServiceConfig.class)
				.withPropertyValues("file.web.enabled:false")
				.run((context) -> assertThrows(NoSuchBeanDefinitionException.class,
						() -> context.getBean(FileWebAutoConfiguration.WebConfiguration.class)));
	}


	@Configuration
	static class MockFileServiceConfig {
		@Bean
		FileService myFileService() {
			return mock(FileService.class);
		}
	}

	@Configuration
	static class MockMetadataServiceConfig {
		@Bean
		FileMetadataService myMetadataService() {
			return mock(FileMetadataService.class);
		}
	}

	@Configuration
	static class MockDataSourceConfig {
		@Bean
		public DataSource dataSource() {
			return mock(DataSource.class);
		}

		@Bean
		public EntityManagerFactory entityManagerFactory() {
			return mock(EntityManagerFactory.class);
		}
	}

	@Configuration
	@AutoConfigurationPackage
	static class BasePackageConfig {
	}
}