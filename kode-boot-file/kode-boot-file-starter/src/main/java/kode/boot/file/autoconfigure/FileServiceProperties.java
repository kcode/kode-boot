package kode.boot.file.autoconfigure;

import kode.boot.file.local.LocalFileProperties;
import kode.boot.file.web.FileWebProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * FileServiceProperties，文件服务配置
 *
 * @author Administrator
 * @date 2019/1/2
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "file")
public class FileServiceProperties {

	@NestedConfigurationProperty
	private LocalFileProperties local = new LocalFileProperties();

	@NestedConfigurationProperty
	private FileWebProperties web = new FileWebProperties();
}
