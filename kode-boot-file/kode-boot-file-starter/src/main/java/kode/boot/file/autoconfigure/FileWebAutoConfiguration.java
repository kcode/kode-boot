package kode.boot.file.autoconfigure;

import kode.boot.file.api.FileMetadataService;
import kode.boot.file.api.FileService;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * FileServiceAutoConfiguration
 *
 * @author Stark
 * @date 2019/1/4
 */
@SuppressWarnings("rawtypes")
@Configuration
@ConditionalOnBean({FileService.class, FileMetadataService.class})
@ConditionalOnProperty(prefix = "file.web", name = "enabled", havingValue = "true", matchIfMissing = true)
@AutoConfigureAfter(FileServiceAutoConfiguration.class)
public class FileWebAutoConfiguration {

	@Configuration
	@ComponentScan("kode.boot.file.web")
	static class WebConfiguration {

	}
}
