package kode.boot.file.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import kode.base.core.util.FileUtil;
import kode.base.core.util.ResourceUtil;
import kode.base.web.util.WebUtil;
import kode.boot.file.api.FileMetadataService;
import kode.boot.file.api.FileService;
import kode.boot.file.local.LocalFileProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * FileControllerTest
 *
 * @author Stark
 * @date 2019/1/13
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(FileController.class)
@ContextConfiguration(classes = TestConfig.class)
class FileControllerTest {

	@MockBean
	private FileMetadataService metadataService;
	@SpyBean
	private LocalFileProperties localFileProperties;
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private FileService fileService;

	String getSampleFilePath(String testFileName) throws URISyntaxException {
		URL url = ResourceUtil.getResource(this.getClass().getClassLoader(), testFileName);
		return FileUtil.slashPath(new File(url.toURI()).getAbsolutePath());
	}

	@Test
	void downloadText_get() throws Exception {
		String fileName = "test";
		mockMvc.perform(get("/file/text/" + fileName).param("text", "Sample")).andDo(print())
				.andExpect(status().isOk())
				.andExpect(header().string(WebUtil.CONTENT_DISPOSITION, String.format(WebUtil.ATTACHMENT_FILENAME_FORMAT, fileName)))
				.andExpect(content().string("Sample"));

		fileName = "test.txt";
		mockMvc.perform(get("/file/text/" + fileName).param("text", "Sample")).andDo(print())
				.andExpect(status().isOk())
				.andExpect(header().string(WebUtil.CONTENT_DISPOSITION, String.format(WebUtil.ATTACHMENT_FILENAME_FORMAT, fileName)))
				.andExpect(content().string("Sample"));
	}

	@Test
	void downloadText_post() throws Exception {
		String fileName = "test.txt";
		mockMvc.perform(post("/file/text/" + fileName).param("text", "Sample")).andDo(print())
				.andExpect(status().isOk())
				.andExpect(header().string(WebUtil.CONTENT_DISPOSITION, String.format(WebUtil.ATTACHMENT_FILENAME_FORMAT, fileName)))
				.andExpect(content().string("Sample"));
	}

	@Test
	void downloadById() throws Exception {
		TestMetadata metadata = new TestMetadata();
		metadata.setName("sample1.jpg");
		metadata.setLocation(getSampleFilePath("sample1.jpg"));
		when(localFileProperties.getBasePath()).thenReturn("");
		when(metadataService.getById(any())).thenReturn(Optional.of(metadata));

		mockMvc.perform(get("/file/id/1")).andDo(print())
				.andExpect(status().isOk())
				.andExpect(header().string(WebUtil.CONTENT_DISPOSITION, String.format(WebUtil.ATTACHMENT_FILENAME_FORMAT, "sample1.jpg")))
		;
	}

	@Test
	void downloadByPath() throws Exception {
		String sampleName = "sample1.jpg";
		String sampleFile = getSampleFilePath(sampleName);
		String sampleFolder = FileUtil.getCurrentFolder(sampleFile);

		TestMetadata metadata = new TestMetadata();
		metadata.setName(sampleName);
		metadata.setLocation(sampleName);
		when(localFileProperties.getBasePath()).thenReturn(sampleFolder);

		mockMvc.perform(get("/file/p/" + sampleName)).andDo(print())
				.andExpect(status().isOk())
				.andExpect(header().string("Content-Length", "22771"))
				.andExpect(header().string("Content-Type", "image/jpeg"))
				.andExpect(header().string("Content-Disposition", String.format(WebUtil.ATTACHMENT_FILENAME_FORMAT, sampleName)))
		;
	}

	@Test
	void upload() throws Exception {
		TestMetadata metadata = new TestMetadata();
		when(metadataService.newInstance()).thenReturn(metadata);
		when(metadataService.saveOrUpdate(any())).thenReturn(metadata);

		MockMultipartHttpServletRequestBuilder builder = multipart("/file/upload")
				.file(new MockMultipartFile("file", "sample1.jpg", "image/jpeg",
						ResourceUtil.getResourceAsStream(this.getClass().getClassLoader(), "sample1.jpg")));

		mockMvc.perform(builder).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.code").value(0))
				.andExpect(jsonPath("$.data").exists())
				.andExpect(jsonPath("$.data.location").exists())
		;
	}

	@Test
	void upload_many() throws Exception {
		TestMetadata metadata = new TestMetadata(), metadata1 = new TestMetadata();
		when(metadataService.newInstance()).thenReturn(metadata, metadata1);
		when(metadataService.saveOrUpdate(any())).thenReturn(metadata, metadata1);

		MockMultipartHttpServletRequestBuilder builder = multipart("/file/upload")
				.file(new MockMultipartFile("files", "sample1.jpg", "image/jpeg",
						ResourceUtil.getResourceAsStream(this.getClass().getClassLoader(), "sample1.jpg")))
				.file(new MockMultipartFile("files", "sample2.jpg", "image/jpeg",
						ResourceUtil.getResourceAsStream(this.getClass().getClassLoader(), "sample2.jpg")));

		mockMvc.perform(builder).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.code").value(0))
				.andExpect(jsonPath("$..location").isArray())
		;
	}

	@Test
	void uploadStatic() throws Exception {
		when(metadataService.newInstance()).then((t) -> new TestMetadata());

		MockMultipartHttpServletRequestBuilder builder = multipart("/file/upload/static")
				.file(new MockMultipartFile("file", "sample1.jpg", "image/jpeg",
						ResourceUtil.getResourceAsStream(this.getClass().getClassLoader(), "sample1.jpg")));

		mockMvc.perform(builder).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.code").value(0))
				.andExpect(jsonPath("$.data").exists())
		;
	}

	@Test
	void uploadStatic_many() throws Exception {
		TestMetadata metadata = new TestMetadata(), metadata1 = new TestMetadata();
		when(metadataService.newInstance()).thenReturn(metadata, metadata1);
		when(metadataService.saveOrUpdate(any())).thenReturn(metadata, metadata1);

		MockMultipartHttpServletRequestBuilder builder = multipart("/file/upload/static")
				.file(new MockMultipartFile("files", "sample1.jpg", "image/jpeg",
						ResourceUtil.getResourceAsStream(this.getClass().getClassLoader(), "sample1.jpg")))
				.file(new MockMultipartFile("files", "sample2.jpg", "image/jpeg",
						ResourceUtil.getResourceAsStream(this.getClass().getClassLoader(), "sample2.jpg")));

		mockMvc.perform(builder).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.code").value(0))
				.andExpect(jsonPath("$.data").exists())
				.andExpect(jsonPath("$.data").isArray())
		;
	}

	@Test
	void jsonTest() throws IOException {
		String str = "[" +
				"{\"name\":\"readme.md\",\"text\":\"test md\"}," +
				"{\"name\":\"logo.png\",\"file\":\"id or location\"}," +
				"{\"name\":\"site.ico\",\"base64\":\"base64chars=\"}" +
				"]";

		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode temp = objectMapper.readTree("{\"name\":\"Mahesh Kumar\", \"age\":21,\"verified\":false,\"marks\": [100,90,85]}");
		System.out.println(temp);

		JsonNode node = objectMapper.readTree(str);

		System.out.println(node);
		List<Map<String, String>> map = objectMapper.readValue(str, new TypeReference<List<Map<String, String>>>() {
		});
		System.out.println(map);
	}
}