package kode.boot.file.web;

import kode.boot.file.api.FileMetadata;
import lombok.Data;

/**
 * TestMetadata
 *
 * @author Stark
 * @date 2019/1/21
 */
@Data
public class TestMetadata implements FileMetadata {
	private Object id;
	private Object createdBy;
	private String location;
	private long size;
	private String extension;
	private String name;
}
