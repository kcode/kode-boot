package kode.boot.file.web;

import kode.boot.file.api.FileMetadataService;
import kode.boot.file.api.FileService;
import kode.boot.file.local.LocalFileProperties;
import kode.boot.file.local.LocalFileService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * TestConfig
 *
 * @author Stark
 * @date 2019/1/19
 */
@Configuration
@ComponentScan("kode.boot.file.web")
public class TestConfig {

//	@Bean
//	public LocalFileProperties localFileProperties() {
//		LocalFileProperties properties =  new LocalFileProperties();
////		properties.setBasePath();
//		return properties;
//	}


	@Bean
	public FileService fileService(FileMetadataService metadataService, LocalFileProperties localFileProperties) {
		LocalFileService fs = new LocalFileService();
		fs.setLocalFileProperties(localFileProperties);
		fs.setFileMetadataService(metadataService);
		return fs;
	}
}
