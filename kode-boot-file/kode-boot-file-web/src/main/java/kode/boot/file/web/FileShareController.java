package kode.boot.file.web;

import io.swagger.annotations.ApiOperation;
import kode.base.core.AppResult;
import org.springframework.web.bind.annotation.*;

/**
 * FileShareController file share
 *
 * @author Stark
 * @date 2019/1/23
 */
@RestController
public class FileShareController {

	//TODO: 文件分享功能

	@GetMapping("/s/{url}")
	@ApiOperation(value = "通过分享路径下载文件，GET 为无提取码方式", httpMethod = "GET")
	public AppResult<?> getShareFile(@PathVariable String url) {
		return AppResult.success();
	}

	@PostMapping("/s/{url}")
	@ApiOperation(value = "通过分享路径下载文件，POST 为提取码方式", httpMethod = "POST")
	public AppResult<?> postShareFile(@PathVariable String url, @RequestParam String code) {
		return AppResult.success();
	}

}
