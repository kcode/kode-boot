package kode.boot.file.web;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * FileWebProperties
 *
 * @author Stark
 * @date 2019/1/5
 */
@Getter
@Setter
@NoArgsConstructor
public class FileWebProperties {
	private boolean enabled = true;
}
