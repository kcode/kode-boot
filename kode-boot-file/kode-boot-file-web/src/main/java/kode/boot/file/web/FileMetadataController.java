package kode.boot.file.web;

import kode.base.core.AppResult;
import kode.boot.file.api.FileMetadata;
import kode.boot.file.api.FileMetadataService;
import kode.boot.file.api.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * FileController
 *
 * @author Stark
 * @date 2019/1/5
 */
@RestController
@RequestMapping("${mapping.file-metadata:/file-meta}")
public class FileMetadataController<T extends FileMetadata<ID>, ID> {

	@Autowired
	private FileMetadataService<T, ID> metadataService;
	@Autowired
	private FileService<T, ID> fileService;

	@GetMapping("/{id}")
	public AppResult<?> readFileById(@PathVariable ID id) {
		return AppResult.success(id);
	}


}
