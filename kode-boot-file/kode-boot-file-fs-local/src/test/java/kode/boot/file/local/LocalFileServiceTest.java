package kode.boot.file.local;

import kode.base.core.util.FileUtil;
import kode.base.core.util.RandomUtil;
import kode.base.core.util.ResourceUtil;
import kode.boot.file.api.FileException;
import kode.boot.file.api.FileMetadata;
import kode.boot.file.api.FileMetadataService;
import lombok.Data;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * LocalFileServiceTest
 *
 * @author Stark
 * @date 2019/1/13
 */
@ExtendWith(MockitoExtension.class)
class LocalFileServiceTest {

	@Mock
	FileMetadataService metadataService;
	@Spy
	LocalFileProperties localFileProperties;
	@InjectMocks
	private LocalFileService fileService = new TestLocalFileService();
	@InjectMocks
	private LocalFileService originalFileService = new LocalFileService();


	private String sampleFileFolder = getSampleFolder() + "/test";
	private String sampleFileName = "sample_test.jpg";

	LocalFileServiceTest() throws URISyntaxException {
	}

	String getSampleFilePath() throws URISyntaxException {
		URL url = ResourceUtil.getResource(this.getClass().getClassLoader(), "sample1.jpg");
		File file = new File(url.toURI());
		return FileUtil.slashPath(file.getAbsolutePath());
	}

	String getSampleFolder() throws URISyntaxException {
		return FileUtil.getCurrentFolder(getSampleFilePath());
	}

	@Test
	void getFileSize() throws URISyntaxException, IOException {
		when(localFileProperties.getBasePath()).thenReturn("");

		String path = getSampleFilePath();
		System.out.println(path);
		long fileSize = fileService.getFileSize(path);

		assertTrue(fileSize > 0);
		assertThrows(FileException.class, () -> fileService.getFileSize(RandomUtil.randomHex(5)));
	}

	@Test
	void readFile() throws URISyntaxException, IOException {
		when(localFileProperties.getBasePath()).thenReturn("");
		InputStream is = fileService.readFile(getSampleFilePath());
		assertNotNull(is);
		assertTrue(is.available() > 0);

		assertThrows(FileException.class, () -> fileService.readFile(RandomUtil.randomHex(5)));
	}

	@Test
	void readFile_id() throws URISyntaxException, IOException {
		when(localFileProperties.getBasePath()).thenReturn("");
		when(metadataService.getById(any())).thenReturn(null);
		assertThrows(NullPointerException.class, () -> fileService.readFile((Object) null));

		TestMetadata metadata = new TestMetadata();
		metadata.setLocation(getSampleFilePath());

		Object id = new Object();
		when(metadataService.getById(id)).thenReturn(Optional.of(metadata));
		InputStream is = fileService.readFile(id);
		assertNotNull(is);
		assertTrue(is.available() > 0);
	}

	@Test
	void readFile_meta() throws URISyntaxException, IOException {
		when(localFileProperties.getBasePath()).thenReturn("");
		TestMetadata metadata = new TestMetadata();
		metadata.setLocation(getSampleFilePath());

		InputStream is = fileService.readFile(metadata);
		assertNotNull(is);
		assertTrue(is.available() > 0);
	}

	@Test
	void delete_afterCopy() throws URISyntaxException, IOException {
		String samplePath = getSampleFilePath();
		String filePath = samplePath.replace("sample1", "temp");
		try (FileOutputStream os = new FileOutputStream(filePath)) {
			Files.copy(Path.of(samplePath), os);
		}

		when(localFileProperties.getBasePath()).thenReturn("");

		System.out.println(filePath);
		Path path = Path.of(filePath);
		assertTrue(Files.exists(path));
		fileService.deleteFile(filePath);
		assertFalse(Files.exists(path));
	}

	@Test
	void saveFile_noValidator() throws URISyntaxException, IOException {
		String sampleFilePath = getSampleFilePath();
		System.out.println("sample path: " + sampleFilePath);
		System.out.println("save folder: " + sampleFileFolder);
		System.out.println("save filename: " + sampleFileName);

		InputStream is = new FileInputStream(sampleFilePath);

		TestMetadata metadata = new TestMetadata();
		metadata.setName("sample1.jpg");

		String target = sampleFileFolder + "/" + sampleFileName;

		when(localFileProperties.getBasePath()).thenReturn("");
		assertFalse(Files.exists(Path.of(target)));

		fileService.saveFile(is, metadata, null);
		assertTrue(metadata.getSize() > 0);
		assertTrue(Files.exists(Path.of(target)));

		fileService.deleteFile(target);
	}

	@Test
	void saveFile_noValidator_original() throws URISyntaxException, IOException {
		String sampleFilePath = getSampleFilePath();
		System.out.println("sample path: " + sampleFilePath);

		InputStream is = new FileInputStream(sampleFilePath);
		TestMetadata metadata = new TestMetadata();
		metadata.setName("sample1.jpg");

		String tempFolder = "d:/dfs";
		when(localFileProperties.getBasePath()).thenReturn(tempFolder);

		originalFileService.saveFile(is, metadata, null);
		String fileLocation = tempFolder + metadata.getLocation();
		System.out.println(fileLocation);
		assertTrue(metadata.getSize() > 0);
		assertTrue(Files.exists(Path.of(fileLocation)));

		assertTrue(originalFileService.deleteFile(metadata.getLocation()));
	}

	@Data
	static class TestMetadata implements FileMetadata {
		private Object id;
		private Object createdBy;
		private String location;
		private long size;
		private String extension;
		private String name;
	}

	class TestLocalFileService extends LocalFileService {
		@Override
		protected String generateFinalFileName(FileMetadata fileMetadata) {
			return sampleFileName;
		}

		@Override
		protected String generateFinalFolderPath(FileMetadata fileMetadata) {
			return sampleFileFolder;
		}
	}
}