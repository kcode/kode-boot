package kode.boot.file.local;

import kode.base.core.util.SystemUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * LocalFileProperties
 *
 * @author Stark
 * @date 2019/1/5
 */
@Getter
@Setter
public class LocalFileProperties {

	/**
	 * base path to save uploaded file
	 */
	private String basePath = SystemUtil.USER_HOME + "/files";

//	/**
//	 * static path for static file
//	 */
//	private String staticPath = SystemUtil.USER_HOME;

}
